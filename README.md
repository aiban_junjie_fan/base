# base

#### 介绍
基础的SpringBoot后台服务,包含基本后台菜单管理、角色管理、权限管理已经后台操作日志。

#### 技术选型：
- 核心框架：Spring Boot 2.1
- 安全框架：Apache Shiro 1.4
- 视图框架：Spring MVC 5.0
- 持久层框架：Mybatis-Plus 3.1.1
- 数据库连接池：Druid 1.1.10
- 日志管理：SLF4J 1.7、Log4j
- 页面交互：Vue2.x
- 工具类：Hutool 4.4.5
- API管理：Swagger 2.9.2

#### 项目结构
```
base
├─file  项目相关文件
│
├─common 公共模块
│  ├─annotation 自定义注解
│  ├─aspect 系统日志
│  ├─base 基类
│  ├─enums 枚举管理
│  ├─exception 异常处理
│  ├─mp Mybatis-Plus填充器
│  ├─utils 常用工具类
│  ├─validator 后台校验
│  └─xss XSS过滤
│ 
├─config 配置信息
│ 
├─modules 功能模块
│  ├─website 接口模块
│  └─sys 后台管理模块
│ 
├─BaseApplication 项目启动类
│  
├──resources 
│  ├─mapper SQL对应的XML文件
│  └─static 静态资源

```

#### 后端部署
- 通过git下载源码
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库base，数据库编码为UTF-8
- 执行file/base.sql文件，初始化数据
- 修改application-dev.yml，更新MySQL账号和密码
- Eclipse、IDEA运行BaseApplication.java，则可启动项目
- Swagger路径：http://localhost:8089/manage/api/swagger-ui.html
- 初始账号密码：admin/admin


#### 代码规范

1. 代码分层:controller层不写业务,只进行数据的转换、组装.将具体业务下放到service进行处理
2. 入参管理:GET请求参数超过3个则建立对应的VO类进行接受，参数校验使用Hibernate Validator校验
3. 出参管理:结果值返回客户端时使用Result.ok(data),直接将参数放在data里面返回
4. 其他待补充

#### todo
-[ ] 完善部署方法
