package cn.iclass;

import cn.iclass.modules.website.utils.JwtUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtTest {
	@Autowired
	private JwtUtils jwtUtils;

	@Test
	public void test() {
		String token = jwtUtils.generateToken(1);

		System.out.println(token);
	}

	@Test
	public void testPwd() {
		String pwd = new Sha256Hash("admin", "YzcmCZNvbXocrsz9dm8e").toHex();

		System.out.println(pwd);
		System.out.println("ec53fed4eb915c8a2edbbad4493b4a6578dc59288b6f104a0acc5db5d1d27c26");
	}

}
