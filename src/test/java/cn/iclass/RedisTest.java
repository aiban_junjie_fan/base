package cn.iclass;

import cn.iclass.modules.sys.entity.SysUserEntity;
import cn.iclass.modules.sys.service.RedisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
	@Resource
	private RedisService redisService;

	@Test
	public void contextLoads() {
		SysUserEntity user = new SysUserEntity();
		user.setName("qqq");
		user.setUserId(1L);
		redisService.set("user", user);
		SysUserEntity user1 = new SysUserEntity();
		user1.setName("qq2");
		user1.setUserId(2L);
		redisService.lPush("userList", user);
		redisService.lPush("userList", user1);

		System.out.println(redisService.get("user", SysUserEntity.class));
		System.out.println(redisService.lSize("userList"));
		System.out.println(redisService.lIndex("userList", 1));
	}

}                               
