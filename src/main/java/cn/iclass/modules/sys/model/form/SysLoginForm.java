package cn.iclass.modules.sys.model.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description: 登录表单
 * @Author: alan
 * @Date: 2019/6/10 5:54 PM
 */
@Data
public class SysLoginForm {
	@NotBlank
	private String username;
	@NotBlank
	private String password;
	@NotBlank
	private String captcha;
	@NotBlank
	private String uuid;
}
