package cn.iclass.modules.sys.model.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @Description: 密码表单
 * @Author: alan
 * @Date: 2019/6/10 5:54 PM
 */
@Data
public class PasswordForm {
	/**
	 * 原密码
	 */
	@NotBlank(message = "原密码不为能空")
	private String password;
	/**
	 * 新密码
	 */
	@NotBlank(message = "新密码不为能空")
	private String newPassword;
}
