package cn.iclass.modules.sys.model.result;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Description: EnumResult.java
 * @Author: alan
 * @Date: 2019/6/10 3:51 PM
 */
@Data
@ApiModel
public class EnumResult {
	private String name;
	private String code;
	private String desc;

}
