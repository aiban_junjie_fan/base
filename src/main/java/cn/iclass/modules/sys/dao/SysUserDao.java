package cn.iclass.modules.sys.dao;

import cn.iclass.common.base.MyBaseMapper;
import cn.iclass.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 系统用户
 *
 * @author alan
 * @date 2016年9月18日 上午9:34:11
 */
@Mapper
public interface SysUserDao extends MyBaseMapper<SysUserEntity> {

	/**
	 * 查询用户的所有权限
	 *
	 * @param userId
	 * @return java.util.List<java.lang.String>
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	List<String> queryAllPerms(Long userId);

	/**
	 * 查询用户的所有菜单ID
	 *
	 * @param userId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 *
	 * @param username
	 * @return cn.iclass.modules.sys.entity.SysUserEntity
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	SysUserEntity queryByUserName(String username);

}
