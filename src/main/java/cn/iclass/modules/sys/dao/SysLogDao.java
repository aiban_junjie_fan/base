package cn.iclass.modules.sys.dao;


import cn.iclass.common.base.MyBaseMapper;
import cn.iclass.modules.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 * @author alan
 * @date 2017-03-08 10:40:56
 */
@Mapper
public interface SysLogDao extends MyBaseMapper<SysLogEntity> {

}
