package cn.iclass.modules.sys.dao;

import cn.iclass.common.base.MyBaseMapper;
import cn.iclass.modules.sys.entity.SysRoleMenuEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 * @author alan
 * @date 2016年9月18日 上午9:33:46
 */
@Mapper
public interface SysRoleMenuDao extends MyBaseMapper<SysRoleMenuEntity> {

	/**
	 * 根据角色ID，获取菜单ID列表
	 *
	 * @param roleId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 *
	 * @param roleIds
	 * @return int
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	int deleteBatch(Long[] roleIds);
}
