package cn.iclass.modules.sys.dao;


import cn.iclass.common.base.MyBaseMapper;
import cn.iclass.modules.sys.entity.SysConfigEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统配置信息
 *
 * @author alan
 * @date 2016年12月4日 下午6:46:16
 */
@Mapper
public interface SysConfigDao extends MyBaseMapper<SysConfigEntity> {

	/**
	 * 根据key，查询value
	 *
	 * @param paramKey
	 * @return cn.iclass.modules.sys.entity.SysConfigEntity
	 * @Author alan
	 * @Date 2019/6/11 10:18 AM
	 */
	SysConfigEntity queryByKey(String paramKey);

	/**
	 * 根据key，更新value
	 *
	 * @param paramKey
	 * @param paramValue
	 * @return int
	 * @Author alan
	 * @Date 2019/6/11 10:18 AM
	 */
	int updateValueByKey(@Param("paramKey") String paramKey, @Param("paramValue") String paramValue);

}
