package cn.iclass.modules.sys.dao;

import cn.iclass.common.base.MyBaseMapper;
import cn.iclass.modules.sys.entity.SysUserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author alan
 * @date 2016年9月18日 上午9:34:46
 */
@Mapper
public interface SysUserRoleDao extends MyBaseMapper<SysUserRoleEntity> {

	/**
	 * 根据用户ID，获取角色ID列表
	 *
	 * @param userId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	List<Long> queryRoleIdList(Long userId);


	/**
	 * 根据角色ID数组，批量删除
	 *
	 * @param roleIds
	 * @return int
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	int deleteBatch(Long[] roleIds);

}
