package cn.iclass.modules.sys.dao;

import cn.iclass.common.base.MyBaseMapper;
import cn.iclass.modules.sys.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 菜单管理
 *
 * @author alan
 * @date 2016年9月18日 上午9:33:01
 */
@Mapper
public interface SysMenuDao extends MyBaseMapper<SysMenuEntity> {

	/**
	 * 根据父菜单，查询子菜单
	 *
	 * @param parentId
	 * @return java.util.List<cn.iclass.modules.sys.entity.SysMenuEntity>
	 * @Author alan
	 * @Date 2019/6/11 10:17 AM
	 */
	List<SysMenuEntity> queryListParentId(Long parentId);

	/**
	 * 获取不包含按钮的菜单列表
	 *
	 * @param
	 * @return java.util.List<cn.iclass.modules.sys.entity.SysMenuEntity>
	 * @Author alan
	 * @Date 2019/6/11 10:18 AM
	 */
	List<SysMenuEntity> queryNotButtonList();

}
