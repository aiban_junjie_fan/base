package cn.iclass.modules.sys.controller;

import cn.iclass.common.annotation.SysLog;
import cn.iclass.common.constant.CommonConstant;
import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.common.utils.Result;
import cn.iclass.common.utils.ShiroUtils;
import cn.iclass.modules.sys.entity.SysRoleEntity;
import cn.iclass.modules.sys.service.SysRoleMenuService;
import cn.iclass.modules.sys.service.SysRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色管理
 *
 * @author alan
 * @date 2016年11月8日 下午2:18:33
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	/**
	 * 角色列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:role:list")
	public Result<PageUtils<SysRoleEntity>> list(BaseQueryParam param,
												 @RequestParam(required = false) String roleName,
												 @RequestParam(required = false) Long createUserId) {
		//如果不是超级管理员，则只查询自己创建的角色列表
		if (ShiroUtils.getUserId() != CommonConstant.SUPER_ADMIN) {
			createUserId = ShiroUtils.getUserId();
		}

		PageUtils page = sysRoleService.queryPage(param, roleName, createUserId);

		return Result.ok(page);
	}

	/**
	 * 角色列表
	 */
	@GetMapping("/select")
	@RequiresPermissions("sys:role:select")
	public Result<Collection<SysRoleEntity>> select() {
		Map<String, Object> map = new HashMap<>();

		//如果不是超级管理员，则只查询自己所拥有的角色列表
		if (ShiroUtils.getUserId() != CommonConstant.SUPER_ADMIN) {
			map.put("createUserId", ShiroUtils.getUserId());
		}
		Collection<SysRoleEntity> list = sysRoleService.listByMap(map);

		return Result.ok(list);
	}

	/**
	 * 角色信息
	 */
	@GetMapping("/info/{roleId}")
	@RequiresPermissions("sys:role:info")
	public Result<SysRoleEntity> info(@PathVariable("roleId") Long roleId) {
		SysRoleEntity role = sysRoleService.getById(roleId);

		//查询角色对应的菜单
		List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);

		return Result.ok(role);
	}

	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@PostMapping("/save")
	@RequiresPermissions("sys:role:save")
	public Result save(@RequestBody @Validated SysRoleEntity role) {

		role.setCreateUserId(ShiroUtils.getUserId());
		sysRoleService.saveRole(role);

		return Result.ok();
	}

	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@PostMapping("/update")
	@RequiresPermissions("sys:role:update")
	public Result update(@RequestBody @Validated SysRoleEntity role) {
		role.setCreateUserId(ShiroUtils.getUserId());
		sysRoleService.update(role);

		return Result.ok();
	}

	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@PostMapping("/delete")
	@RequiresPermissions("sys:role:delete")
	public Result delete(@RequestBody Long[] roleIds) {
		sysRoleService.deleteBatch(roleIds);

		return Result.ok();
	}
}
