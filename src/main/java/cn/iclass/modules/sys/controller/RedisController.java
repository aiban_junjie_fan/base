package cn.iclass.modules.sys.controller;


import cn.iclass.common.constant.RedisKeys;
import cn.iclass.common.utils.Result;
import cn.iclass.modules.sys.entity.SysUserEntity;
import cn.iclass.modules.sys.service.RedisService;
import cn.iclass.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Redis测试
 *
 * @author alan
 * @date 2016年12月4日 下午6:55:53
 */
@Api(tags = "RedisController")
@RestController
@RequestMapping("/sys/redis")
public class RedisController {
	@Resource
	private RedisService redisService;
	@Resource
	private SysUserService sysUserService;


	@ApiOperation("测试简单缓存")
	@GetMapping(value = "/simpleTest")
	public Result<SysUserEntity> simpleTest() {
		List<SysUserEntity> userList = sysUserService.list();
		SysUserEntity sysUser = userList.get(0);
		String key = RedisKeys.SIMPLE_TEST + sysUser.getUserId();
		redisService.set(key, sysUser);
		SysUserEntity cacheSysUser = (SysUserEntity) redisService.get(key);
		return Result.ok(cacheSysUser);
	}

	@GetMapping(value = "/get")
	@Cacheable(value = RedisKeys.DATABASE, key = "'simple:test:'+#id", unless = "#result==null")
	public SysUserEntity getItem(Long id) {
		return sysUserService.getById(id);
	}

}
