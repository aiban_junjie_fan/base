package cn.iclass.modules.sys.controller;

import cn.iclass.common.annotation.SysLog;
import cn.iclass.common.constant.CommonConstant;
import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.common.utils.Result;
import cn.iclass.common.utils.ShiroUtils;
import cn.iclass.common.validator.group.Add;
import cn.iclass.common.validator.group.Update;
import cn.iclass.modules.sys.entity.SysUserEntity;
import cn.iclass.modules.sys.model.form.PasswordForm;
import cn.iclass.modules.sys.service.SysUserRoleService;
import cn.iclass.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: 系统用户
 * @Author: alan
 * @Date: 2018/11/19 9:34 AM
 */
@RestController
@RequestMapping("/sys/user")
@Api("系统用户")
public class SysUserController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;


	/**
	 * 所有用户列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:user:list")
	@ApiOperation("所有用户列表")
	public Result<PageUtils<SysUserEntity>> list(BaseQueryParam param,
												 @RequestParam(required = false) String username,
												 @RequestParam(required = false) Long createUserId) {
		//只有超级管理员，才能查看所有管理员列表
		if (ShiroUtils.getUserId() != CommonConstant.SUPER_ADMIN) {
			createUserId = ShiroUtils.getUserId();
		}
		PageUtils page = sysUserService.queryPage(param, username, createUserId);

		return Result.ok(page);
	}

	/**
	 * 获取登录的用户信息
	 */
	@GetMapping("/info")
	@ApiOperation("获取登录的用户信息")
	public Result<SysUserEntity> info() {
		return Result.ok(ShiroUtils.getUser());
	}

	/**
	 * 修改登录用户密码
	 */
	@SysLog("修改密码")
	@PostMapping("/password")
	@ApiOperation("修改密码")
	public Result password(@RequestBody @Validated PasswordForm form) {
		//sha256加密
		String password = new Sha256Hash(form.getPassword(), ShiroUtils.getUser().getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(form.getNewPassword(), ShiroUtils.getUser().getSalt()).toHex();

		//更新密码
		boolean flag = sysUserService.updatePassword(ShiroUtils.getUserId(), password, newPassword);
		if (!flag) {
			return Result.error("原密码不正确");
		}

		return Result.ok();
	}

	/**
	 * 用户信息
	 */
	@GetMapping("/info/{userId}")
	@RequiresPermissions("sys:user:info")
	@ApiOperation("用户信息")
	public Result<SysUserEntity> info(@PathVariable("userId") Long userId) {
		SysUserEntity user = sysUserService.getById(userId);

		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);

		return Result.ok(user);
	}

	/**
	 * 保存用户
	 */
	@SysLog("保存用户")
	@PostMapping("/save")
	@RequiresPermissions("sys:user:save")
	@ApiOperation("保存用户")
	public Result save(@RequestBody @Validated({Add.class}) SysUserEntity user) {
		user.setCreateUserId(ShiroUtils.getUserId());
		sysUserService.saveUser(user);

		return Result.ok();
	}

	/**
	 * 修改用户
	 */
	@SysLog("修改用户")
	@PostMapping("/update")
	@RequiresPermissions("sys:user:update")
	@ApiOperation("修改用户")
	public Result update(@RequestBody @Validated({Update.class}) SysUserEntity user) {
		user.setCreateUserId(ShiroUtils.getUserId());
		sysUserService.update(user);

		return Result.ok();
	}

	/**
	 * 删除用户
	 */
	@SysLog("删除用户")
	@PostMapping("/delete")
	@RequiresPermissions("sys:user:delete")
	@ApiOperation("删除用户")
	public Result delete(@RequestBody Long[] userIds) {
		if (ArrayUtils.contains(userIds, CommonConstant.SUPER_ADMIN)) {
			return Result.error("系统管理员不能删除");
		}

		if (ArrayUtils.contains(userIds, ShiroUtils.getUserId())) {
			return Result.error("当前用户不能删除");
		}

		sysUserService.deleteBatch(userIds);

		return Result.ok();
	}


}
