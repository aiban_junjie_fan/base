package cn.iclass.modules.sys.controller;

import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.common.utils.Result;
import cn.iclass.modules.sys.entity.SysLogEntity;
import cn.iclass.modules.sys.service.SysLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 系统日志
 *
 * @author alan
 * @date 2017-03-08 10:40:56
 */
@Controller
@RequestMapping("/sys/log")
public class SysLogController {
	@Autowired
	private SysLogService sysLogService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("sys:log:list")
	public Result<PageUtils<SysLogEntity>> list(BaseQueryParam param, @RequestParam(required = false) String key) {
		PageUtils page = sysLogService.queryPage(param, key);
		return Result.ok(page);
	}

}
