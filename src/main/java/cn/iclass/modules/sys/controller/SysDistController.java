package cn.iclass.modules.sys.controller;


import cn.hutool.core.util.EnumUtil;
import cn.iclass.common.utils.Result;
import cn.iclass.modules.sys.model.result.EnumResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: SysDistController.java
 * @Author: alan
 * @Date: 2019/6/10 5:12 PM
 */
@RestController
@RequestMapping("/sys/dist")
public class SysDistController {


	/**
	 * 所有配置列表
	 */
	@GetMapping("{distName}/list/")
	public Result<List<EnumResult>> distList(@PathVariable("distName") String distName) throws ClassNotFoundException {
		String enumName = "cn.iclass.common.enums." + distName;
		//以String类型的className实例化类
		Class clazz = Class.forName(enumName);
		List<Object> desc = EnumUtil.getFieldValues(clazz, "desc");
		List<String> names = EnumUtil.getNames(clazz);
		List<Object> values = EnumUtil.getFieldValues(clazz, "value");
		List<EnumResult> enumResults = new ArrayList<>();
		for (int i = 0; i < desc.size(); i++) {
			EnumResult enumResult = new EnumResult();
			enumResult.setCode(String.valueOf(values.get(i)));
			enumResult.setDesc(String.valueOf(desc.get(i)));
			enumResult.setName(String.valueOf(names.get(i)));
			enumResults.add(enumResult);
		}
		return Result.ok(enumResults);
	}

}
