package cn.iclass.modules.sys.controller;


import cn.iclass.common.annotation.SysLog;
import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.common.utils.Result;
import cn.iclass.modules.sys.entity.SysConfigEntity;
import cn.iclass.modules.sys.service.SysConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 系统配置信息
 *
 * @author alan
 * @date 2016年12月4日 下午6:55:53
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController {
	@Autowired
	private SysConfigService sysConfigService;

	/**
	 * 所有配置列表
	 */
	@GetMapping("/list")
	@Cacheable(value = "config", key = "'list'+#paramKey+'_'+#queryParam.current+'_'+#queryParam.size")
	@RequiresPermissions("sys:config:list")
	public Result<PageUtils> list(BaseQueryParam queryParam, @RequestParam(required = false) String paramKey) {
		PageUtils page = sysConfigService.queryPage(queryParam, paramKey);
		return Result.ok(page);
	}

	/**
	 * 配置信息
	 */
	@GetMapping("/info/{id}")
	@Cacheable(value = "config", key = "#id")
	@RequiresPermissions("sys:config:info")
	public Result<SysConfigEntity> info(@PathVariable("id") Long id) {
		SysConfigEntity config = sysConfigService.getById(id);
		return Result.ok(config);
	}

	/**
	 * 保存配置
	 */
	@SysLog("保存配置")
	@CacheEvict(value = "config", allEntries = true)
	@PostMapping("/save")
	@RequiresPermissions("sys:config:save")
	public Result save(@RequestBody @Validated SysConfigEntity config) {
		sysConfigService.saveConfig(config);
		return Result.ok();
	}

	/**
	 * 修改配置
	 */
	@SysLog("修改配置")
	@CacheEvict(value = "config", allEntries = true)
	@PostMapping("/update")
	@RequiresPermissions("sys:config:update")
	public Result update(@RequestBody @Validated SysConfigEntity config) {
		sysConfigService.update(config);
		return Result.ok();
	}

	/**
	 * 删除配置
	 */
	@SysLog("删除配置")
	@CacheEvict(value = "config", allEntries = true)
	@PostMapping("/delete")
	@RequiresPermissions("sys:config:delete")
	public Result delete(@RequestBody Long[] ids) {
		sysConfigService.deleteBatch(ids);
		return Result.ok();
	}

}
