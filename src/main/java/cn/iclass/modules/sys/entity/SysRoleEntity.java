package cn.iclass.modules.sys.entity;


import cn.iclass.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 角色
 * @Author: alan
 * @Date: 2019/6/10 6:01 PM
 */
@Data
@TableName("sys_role")
@EqualsAndHashCode(callSuper = false)
public class SysRoleEntity extends BaseEntity<SysRoleEntity> {

	/**
	 * 角色ID
	 */
	@TableId(type = IdType.AUTO)
	private Long roleId;

	/**
	 * 角色名称
	 */
	@NotBlank(message = "角色名称不能为空")
	private String roleName;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建者ID
	 */
	private Long createUserId;

	@TableField(exist = false)
	private List<Long> menuIdList;


	@Override
	protected Serializable pkVal() {
		return this.roleId;
	}
}
