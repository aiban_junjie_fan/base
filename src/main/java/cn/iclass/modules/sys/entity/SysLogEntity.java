package cn.iclass.modules.sys.entity;


import cn.iclass.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 系统日志
 * @Author: alan
 * @Date: 2019/6/10 6:01 PM
 */
@Data
@TableName("sys_log")
@EqualsAndHashCode(callSuper = false)
public class SysLogEntity extends BaseEntity<SysLogEntity> {
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 用户操作
	 */
	private String operation;
	/**
	 * 请求方法
	 */
	private String method;
	/**
	 * 请求参数
	 */
	private String params;
	/**
	 * 执行时长(毫秒)
	 */
	private Long time;
	/**
	 * IP地址
	 */
	private String ip;
	/**
	 * 创建时间
	 */
	private Date createDate;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
