package cn.iclass.modules.sys.entity;

import cn.iclass.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Description: 系统配置信息
 * @Author: alan
 * @Date: 2019/6/10 6:00 PM
 */
@Data
@TableName("sys_config")
@EqualsAndHashCode(callSuper = false)
public class SysConfigEntity extends BaseEntity<SysConfigEntity> {
	@TableId(type = IdType.AUTO)
	private Long id;
	@NotBlank(message = "参数名不能为空")
	private String paramKey;
	@NotBlank(message = "参数值不能为空")
	private String paramValue;
	private String remark;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
