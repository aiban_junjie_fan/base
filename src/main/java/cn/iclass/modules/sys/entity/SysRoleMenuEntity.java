package cn.iclass.modules.sys.entity;


import cn.iclass.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Description: 角色与菜单对应关系
 * @Author: alan
 * @Date: 2019/6/10 6:01 PM
 */
@Data
@TableName("sys_role_menu")
@EqualsAndHashCode(callSuper = false)
public class SysRoleMenuEntity extends BaseEntity<SysRoleMenuEntity> {

	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 角色ID
	 */
	private Long roleId;

	/**
	 * 菜单ID
	 */
	private Long menuId;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
