package cn.iclass.modules.sys.entity;


import cn.iclass.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Description: 用户与角色对应关系
 * @Author: alan
 * @Date: 2019/6/10 6:01 PM
 */
@Data
@TableName("sys_user_role")
@EqualsAndHashCode(callSuper = false)
public class SysUserRoleEntity extends BaseEntity<SysUserRoleEntity> {
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 角色ID
	 */
	private Long roleId;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
