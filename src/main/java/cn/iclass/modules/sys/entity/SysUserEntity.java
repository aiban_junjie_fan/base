package cn.iclass.modules.sys.entity;

import cn.iclass.common.base.BaseEntity;
import cn.iclass.common.enums.UserStatusEnum;
import cn.iclass.common.validator.group.Add;
import cn.iclass.common.validator.group.Update;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 系统用户
 * @Author: alan
 * @Date: 2019/6/10 6:01 PM
 */
@Data
@TableName("sys_user")
@EqualsAndHashCode(callSuper = false)
public class SysUserEntity extends BaseEntity<SysUserEntity> {

	/**
	 * 用户ID
	 */
	@TableId(type = IdType.AUTO)
	private Long userId;

	/**
	 * 用户名
	 */
	@NotBlank(message = "用户名不能为空", groups = {Add.class, Update.class})
	private String username;

	/**
	 * 密码
	 */
	@NotBlank(message = "密码不能为空", groups = Add.class)
	private String password;

	/**
	 * 盐
	 */
	private String salt;


	/**
	 * 真实姓名
	 */
	@ApiModelProperty(value = "真实姓名")
	private String name;

	/**
	 * 电话
	 */
	@ApiModelProperty(value = "电话")
	private String mobile;

	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String avatar;


	/**
	 * 角色ID列表
	 */
	@TableField(exist = false)
	private List<Long> roleIdList;

	/**
	 * 创建者ID
	 */
	private Long createUserId;

	/**
	 * 状态
	 */
	@ApiModelProperty(value = "状态")
	private UserStatusEnum status;


	@Override
	protected Serializable pkVal() {
		return this.userId;
	}
}
