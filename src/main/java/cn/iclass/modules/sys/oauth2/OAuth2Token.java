package cn.iclass.modules.sys.oauth2;


import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Description: token
 * @Author: alan
 * @Date: 2019/6/10 6:00 PM
 */
public class OAuth2Token implements AuthenticationToken {
	private String token;

	public OAuth2Token(String token) {
		this.token = token;
	}

	@Override
	public String getPrincipal() {
		return token;
	}

	@Override
	public Object getCredentials() {
		return token;
	}
}
