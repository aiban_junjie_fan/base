package cn.iclass.modules.sys.service;

import cn.iclass.common.utils.Result;

/**
 * 用户Token
 *
 * @author alan
 * @date 2017-03-23 15:22:07
 */
public interface SysUserTokenService {

	/**
	 * 生成token
	 *
	 * @param userId 用户ID
	 * @return cn.iclass.common.utils.R
	 * @Author alan
	 * @Date 2019/6/10 5:24 PM
	 */
	Result createToken(long userId);

	/**
	 * queryUserIdByToken
	 *
	 * @param token
	 * @return java.lang.Long
	 * @Author alan
	 * @Date 2019/7/5 2:00 PM
	 */
	Long queryUserIdByToken(String token);

	/**
	 * logout
	 *
	 * @param token
	 * @return void
	 * @Author alan
	 * @Date 2019/7/5 2:10 PM
	 */
	void logout(String token);

}
