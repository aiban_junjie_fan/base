package cn.iclass.modules.sys.service;

import cn.iclass.modules.sys.entity.SysUserRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 用户与角色对应关系
 *
 * @author alan
 * @date 2016年9月18日 上午9:43:24
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {

	/**
	 * 保存
	 *
	 * @param userId
	 * @param roleIdList
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:31 PM
	 */
	void saveOrUpdate(Long userId, List<Long> roleIdList);

	/**
	 * 根据用户ID，获取角色ID列表
	 *
	 * @param userId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/10 5:31 PM
	 */
	List<Long> queryRoleIdList(Long userId);

	/**
	 * 根据角色ID数组，批量删除
	 *
	 * @param roleIds
	 * @return int
	 * @Author alan
	 * @Date 2019/6/10 5:31 PM
	 */
	int deleteBatch(Long[] roleIds);
}
