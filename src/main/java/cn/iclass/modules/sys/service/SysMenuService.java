package cn.iclass.modules.sys.service;


import cn.iclass.modules.sys.entity.SysMenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 菜单管理
 *
 * @author alan
 * @date 2016年9月18日 上午9:42:16
 */
public interface SysMenuService extends IService<SysMenuEntity> {

	/**
	 * 根据父菜单，查询子菜单
	 *
	 * @param parentId
	 * @param menuIdList
	 * @return java.util.List<cn.iclass.modules.sys.entity.SysMenuEntity>
	 * @Author alan
	 * @Date 2019/6/10 5:28 PM
	 */
	List<SysMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList);

	/**
	 * 根据父菜单，查询子菜单
	 *
	 * @param parentId
	 * @return java.util.List<cn.iclass.modules.sys.entity.SysMenuEntity>
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	List<SysMenuEntity> queryListParentId(Long parentId);

	/**
	 * 获取不包含按钮的菜单列表
	 *
	 * @param
	 * @return java.util.List<cn.iclass.modules.sys.entity.SysMenuEntity>
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	List<SysMenuEntity> queryNotButtonList();

	/**
	 * 获取用户菜单列表
	 *
	 * @param userId
	 * @return java.util.List<cn.iclass.modules.sys.entity.SysMenuEntity>
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	List<SysMenuEntity> getUserMenuList(Long userId);

	/**
	 * 删除
	 *
	 * @param menuId
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	void delete(Long menuId);
}
