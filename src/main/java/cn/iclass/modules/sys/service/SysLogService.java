package cn.iclass.modules.sys.service;


import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.entity.SysLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 系统日志
 *
 * @author alan
 * @date 2017-03-08 10:40:56
 */
public interface SysLogService extends IService<SysLogEntity> {
	/**
	 * 分页查询
	 *
	 * @param param
	 * @param key
	 * @return cn.iclass.common.utils.PageUtils
	 * @Author alan
	 * @Date 2019/6/11 11:42 AM
	 */
	PageUtils queryPage(BaseQueryParam param, String key);

}
