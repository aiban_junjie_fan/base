package cn.iclass.modules.sys.service;

import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.entity.SysRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 角色
 *
 * @author alan
 * @date 2016年9月18日 上午9:42:52
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	/**
	 * queryPage
	 *
	 * @param param
	 * @param roleName
	 * @param createUserId
	 * @return cn.iclass.common.utils.PageUtils
	 * @Author alan
	 * @Date 2019/6/11 11:55 AM
	 */
	PageUtils queryPage(BaseQueryParam param, String roleName, Long createUserId);

	/**
	 * 新增角色
	 *
	 * @param role
	 * @return boolean
	 * @Author alan
	 * @Date 2019/6/10 5:30 PM
	 */
	boolean saveRole(SysRoleEntity role);

	/**
	 * 更新角色信息
	 *
	 * @param role
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:30 PM
	 */
	void update(SysRoleEntity role);

	/**
	 * 批量删除
	 *
	 * @param roleIds
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:30 PM
	 */
	void deleteBatch(Long[] roleIds);


	/**
	 * 查询用户创建的角色ID列表
	 *
	 * @param createUserId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/10 5:30 PM
	 */
	List<Long> queryRoleIdList(Long createUserId);
}
