package cn.iclass.modules.sys.service;


import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.entity.SysConfigEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统配置信息
 *
 * @author alan
 * @date 2016年12月4日 下午6:49:01
 */
public interface SysConfigService extends IService<SysConfigEntity> {

	/**
	 * 分页查询
	 *
	 * @param param
	 * @param paramKey
	 * @return cn.iclass.common.utils.PageUtils
	 * @Author alan
	 * @Date 2019/6/11 10:45 AM
	 */
	PageUtils queryPage(BaseQueryParam param, String paramKey);

	/**
	 * 保存配置信息
	 *
	 * @param config
	 * @return boolean
	 * @Author alan
	 * @Date 2019/6/10 5:25 PM
	 */
	boolean saveConfig(SysConfigEntity config);

	/**
	 * 更新配置信息
	 *
	 * @param config
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:27 PM
	 */
	void update(SysConfigEntity config);

	/**
	 * 删除配置信息
	 *
	 * @param ids
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:27 PM
	 */
	void deleteBatch(Long[] ids);

	/**
	 * 根据key，获取配置的value值
	 *
	 * @param key
	 * @return java.lang.String
	 * @Author alan
	 * @Date 2019/6/10 5:27 PM
	 */
	String getValue(String key);


}
