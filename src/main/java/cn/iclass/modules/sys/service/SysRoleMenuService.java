package cn.iclass.modules.sys.service;

import cn.iclass.modules.sys.entity.SysRoleMenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 角色与菜单对应关系
 *
 * @author alan
 * @date 2016年9月18日 上午9:42:30
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

	/**
	 * 保存
	 *
	 * @param roleId
	 * @param menuIdList
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	void saveOrUpdate(Long roleId, List<Long> menuIdList);

	/**
	 * 根据角色ID，获取菜单ID列表
	 *
	 * @param roleId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 *
	 * @param roleIds
	 * @return int
	 * @Author alan
	 * @Date 2019/6/10 5:29 PM
	 */
	int deleteBatch(Long[] roleIds);

}
