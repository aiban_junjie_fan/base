package cn.iclass.modules.sys.service;

import cn.hutool.captcha.ICaptcha;

/**
 * 验证码
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2.0.0 2018-02-10
 */
public interface SysCaptchaService {

	/**
	 * 获取图片验证码
	 *
	 * @param uuid
	 * @return cn.hutool.captcha.ICaptcha
	 * @Author alan
	 * @Date 2019/6/11 1:32 PM
	 */
	ICaptcha getCaptcha(String uuid);

	/**
	 * 验证码效验
	 *
	 * @param uuid uuid
	 * @param code 验证码
	 * @return true：成功  false：失败
	 */
	boolean validate(String uuid, String code);
}
