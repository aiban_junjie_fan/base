package cn.iclass.modules.sys.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.iclass.common.constant.CommonConstant;
import cn.iclass.common.enums.UserStatusEnum;
import cn.iclass.common.exception.GlobalException;
import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.dao.SysRoleDao;
import cn.iclass.modules.sys.dao.SysUserDao;
import cn.iclass.modules.sys.entity.SysUserEntity;
import cn.iclass.modules.sys.service.SysUserRoleService;
import cn.iclass.modules.sys.service.SysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


/**
 * 系统用户
 *
 * @author alan
 * @date 2016年9月18日 上午9:46:09
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Resource
	private SysRoleDao sysRoleDao;

	@Override
	public PageUtils queryPage(BaseQueryParam param, String username, Long createUserId) {
		IPage<SysUserEntity> page = this.page(
				new Page<>(param.getCurrent(), param.getSize()),
				new QueryWrapper<SysUserEntity>().lambda()
						.like(StringUtils.isNotBlank(username), SysUserEntity::getUsername, username)
						.eq(createUserId != null, SysUserEntity::getCreateUserId, createUserId)
		);
		return new PageUtils(page);
	}


	@Override
	public List<Long> queryAllMenuId(Long userId) {
		return baseMapper.queryAllMenuId(userId);
	}

	@Override
	public SysUserEntity queryByUserName(String username) {
		return baseMapper.queryByUserName(username);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean saveUser(SysUserEntity user) {
		//sha256加密
		String salt = RandomUtil.randomString(20);
		user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());
		user.setSalt(salt);
		user.setStatus(UserStatusEnum.ENABLED);
		this.save(user);
		//检查角色是否越权
		checkRole(user);
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
		return Boolean.TRUE;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysUserEntity user) {
		if (StringUtils.isBlank(user.getPassword())) {
			user.setPassword(null);
		} else {
			user.setPassword(new Sha256Hash(user.getPassword(), user.getSalt()).toHex());
		}
		this.updateById(user);

		//检查角色是否越权
		checkRole(user);

		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
	}

	@Override
	public void deleteBatch(Long[] userId) {
		this.removeByIds(Arrays.asList(userId));
	}

	@Override
	public boolean updatePassword(Long userId, String password, String newPassword) {
		SysUserEntity userEntity = new SysUserEntity();
		userEntity.setPassword(newPassword);
		return this.update(userEntity,
				new QueryWrapper<SysUserEntity>().lambda()
						.eq(SysUserEntity::getUserId, userId)
						.eq(SysUserEntity::getPassword, password));
	}


	/**
	 * 检查角色是否越权
	 */
	private void checkRole(SysUserEntity user) {
		if (user.getRoleIdList() == null || user.getRoleIdList().size() == 0) {
			return;
		}
		//如果不是超级管理员，则需要判断用户的角色是否自己创建
		if (user.getCreateUserId() == CommonConstant.SUPER_ADMIN) {
			return;
		}

		//查询用户创建的角色列表
		List<Long> roleIdList = sysRoleDao.queryRoleIdList(user.getCreateUserId());

		//判断是否越权
		if (!roleIdList.containsAll(user.getRoleIdList())) {
			throw new GlobalException("新增用户所选角色，不是本人创建");
		}
	}
}
