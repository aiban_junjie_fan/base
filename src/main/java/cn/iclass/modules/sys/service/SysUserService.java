package cn.iclass.modules.sys.service;

import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.entity.SysUserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 系统用户
 *
 * @author alan
 * @date 2016年9月18日 上午9:43:39
 */
public interface SysUserService extends IService<SysUserEntity> {

	/**
	 * 分页查询
	 *
	 * @param param
	 * @param username
	 * @param createUserId
	 * @return cn.iclass.common.utils.PageUtils
	 * @Author alan
	 * @Date 2019/6/11 11:57 AM
	 */
	PageUtils queryPage(BaseQueryParam param, String username, Long createUserId);

	/**
	 * 查询用户的所有菜单ID
	 *
	 * @param userId
	 * @return java.util.List<java.lang.Long>
	 * @Author alan
	 * @Date 2019/6/10 5:23 PM
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 *
	 * @param username
	 * @return cn.iclass.modules.sys.entity.SysUserEntity
	 * @Author alan
	 * @Date 2019/6/10 5:23 PM
	 */
	SysUserEntity queryByUserName(String username);

	/**
	 * 保存用户
	 *
	 * @param user
	 * @return boolean
	 * @Author alan
	 * @Date 2019/6/10 5:23 PM
	 */
	boolean saveUser(SysUserEntity user);

	/**
	 * 修改用户
	 *
	 * @param user
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:23 PM
	 */
	void update(SysUserEntity user);

	/**
	 * 删除用户
	 *
	 * @param userIds
	 * @return void
	 * @Author alan
	 * @Date 2019/6/10 5:22 PM
	 */
	void deleteBatch(Long[] userIds);

	/**
	 * 修改密码
	 *
	 * @param userId
	 * @param password
	 * @param newPassword
	 * @return boolean
	 * @Author alan
	 * @Date 2019/6/10 5:23 PM
	 */
	boolean updatePassword(Long userId, String password, String newPassword);


}
