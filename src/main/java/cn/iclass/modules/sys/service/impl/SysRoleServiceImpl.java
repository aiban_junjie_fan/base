package cn.iclass.modules.sys.service.impl;

import cn.iclass.common.constant.CommonConstant;
import cn.iclass.common.exception.GlobalException;
import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.dao.SysRoleDao;
import cn.iclass.modules.sys.dao.SysRoleMenuDao;
import cn.iclass.modules.sys.dao.SysUserDao;
import cn.iclass.modules.sys.dao.SysUserRoleDao;
import cn.iclass.modules.sys.entity.SysRoleEntity;
import cn.iclass.modules.sys.service.SysRoleMenuService;
import cn.iclass.modules.sys.service.SysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 角色
 * @Author: alan
 * @Date: 2019/6/10 5:27 PM
 */
@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRoleEntity> implements SysRoleService {
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	@Resource
	private SysUserDao sysUserDao;
	@Resource
	private SysUserRoleDao sysUserRoleDao;
	@Resource
	private SysRoleMenuDao sysRoleMenuDao;

	@Override
	public PageUtils queryPage(BaseQueryParam param, String roleName, Long createUserId) {

		IPage<SysRoleEntity> page = this.page(
				new Page<>(param.getCurrent(), param.getSize()),
				new QueryWrapper<SysRoleEntity>().lambda()
						.like(StringUtils.isNotBlank(roleName), SysRoleEntity::getRoleName, roleName)
						.eq(createUserId != null, SysRoleEntity::getCreateUserId, createUserId)
		);

		return new PageUtils(page);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean saveRole(SysRoleEntity role) {
		this.save(role);

		//检查权限是否越权
		checkPrems(role);

		//保存角色与菜单关系
		sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
		return Boolean.TRUE;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysRoleEntity role) {
		this.updateById(role);

		//检查权限是否越权
		checkPrems(role);

		//更新角色与菜单关系
		sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] roleIds) {
		//删除角色
		this.removeByIds(Arrays.asList(roleIds));

		//删除角色与菜单关联
		sysRoleMenuDao.deleteBatch(roleIds);

		//删除角色与用户关联
		sysUserRoleDao.deleteBatch(roleIds);
	}


	@Override
	public List<Long> queryRoleIdList(Long createUserId) {
		return baseMapper.queryRoleIdList(createUserId);
	}

	/**
	 * 检查权限是否越权
	 */
	private void checkPrems(SysRoleEntity role) {
		//如果不是超级管理员，则需要判断角色的权限是否超过自己的权限
		if (role.getCreateUserId() == CommonConstant.SUPER_ADMIN) {
			return;
		}

		//查询用户所拥有的菜单列表
		List<Long> menuIdList = sysUserDao.queryAllMenuId(role.getCreateUserId());

		//判断是否越权
		if (!menuIdList.containsAll(role.getMenuIdList())) {
			throw new GlobalException("新增角色的权限，已超出你的权限范围");
		}
	}
}
