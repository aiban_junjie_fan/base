package cn.iclass.modules.sys.service;

import cn.iclass.modules.sys.entity.SysUserEntity;

import java.util.Set;

/**
 * shiro相关接口
 *
 * @author alan
 * @date 2017-06-06 8:49
 */
public interface ShiroService {
	/**
	 * 获取用户权限列表
	 *
	 * @param userId
	 * @return java.util.Set<java.lang.String>
	 * @Author alan
	 * @Date 2019/6/10 5:24 PM
	 */
	Set<String> getUserPermissions(long userId);


	/**
	 * 根据用户ID，查询用户
	 *
	 * @param userId
	 * @return cn.iclass.modules.sys.entity.SysUserEntity
	 * @Author alan
	 * @Date 2019/6/10 5:25 PM
	 */
	SysUserEntity queryUser(Long userId);
}
