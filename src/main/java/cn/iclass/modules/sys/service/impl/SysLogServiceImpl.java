package cn.iclass.modules.sys.service.impl;

import cn.iclass.common.param.BaseQueryParam;
import cn.iclass.common.utils.PageUtils;
import cn.iclass.modules.sys.dao.SysLogDao;
import cn.iclass.modules.sys.entity.SysLogEntity;
import cn.iclass.modules.sys.service.SysLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;


/**
 * @Description: 系统日志
 * @Author: alan
 * @Date: 2019/6/10 5:25 PM
 */
@Service("sysLogService")
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

	@Override
	public PageUtils queryPage(BaseQueryParam param, String key) {
		IPage<SysLogEntity> page = this.page(
				new Page<>(param.getCurrent(), param.getSize()),
				new QueryWrapper<SysLogEntity>().lambda().like(StringUtils.isNotBlank(key), SysLogEntity::getUsername,
						key)
		);

		return new PageUtils(page);
	}
}
