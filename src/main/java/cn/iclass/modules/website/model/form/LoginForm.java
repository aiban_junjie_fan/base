package cn.iclass.modules.website.model.form;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录表单
 *
 * @author Mark sunlightcs@gmail.com
 * @since 3.1.0 2018-01-25
 */
@Data
@ApiModel(value = "登录表单")
public class LoginForm {
	@NotBlank
	private String username;
	@NotBlank
	private String password;
}
