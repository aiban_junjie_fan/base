package cn.iclass.modules.website.controller;


import cn.iclass.common.utils.Result;
import cn.iclass.modules.sys.entity.SysUserEntity;
import cn.iclass.modules.website.annotation.Login;
import cn.iclass.modules.website.annotation.LoginUser;
import cn.iclass.modules.website.model.form.LoginForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * APP登录授权
 *
 * @author alan
 * @date 2017-03-23 15:31
 */
@RestController
@RequestMapping("/weixin/")
@Api("登录接口")
public class LoginController {


	/**
	 * 登录
	 */
	@PostMapping("login")
	@ApiOperation("登录")
	public Result login(@RequestBody @Validated LoginForm form) {
		return Result.ok();
	}


	@Login
	@GetMapping("userInfo")
	@ApiOperation("获取用户信息")
	public Result userInfo(@LoginUser SysUserEntity user) {
		return Result.ok(user);
	}

	@Login
	@GetMapping("userId")
	@ApiOperation("获取用户ID")
	public Result userInfo(@RequestAttribute("userId") Integer userId) {
		return Result.ok(userId);
	}

	@GetMapping("notToken")
	@ApiOperation("忽略Token验证测试")
	public Result notToken() {
		return Result.msg("无需token也能访问。。。");
	}

}
