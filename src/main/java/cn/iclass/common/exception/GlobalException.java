package cn.iclass.common.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @Description: 自定义异常
 * @author: alan
 * @date: 2018/11/13 1:33 PM
 */
@Data
public class GlobalException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String msg;
	private int code = 500;

	public GlobalException(HttpStatus error) {
		super(error.getReasonPhrase());
		this.code = error.value();
		this.msg = error.getReasonPhrase();
	}

	public GlobalException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public GlobalException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}

	public GlobalException(String msg, int code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}

	public GlobalException(String msg, int code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}
}
