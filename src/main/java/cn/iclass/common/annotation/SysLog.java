package cn.iclass.common.annotation;

import java.lang.annotation.*;

/**
 * @Description: 系统日志注解
 * @Author: alan
 * @Date: 2019/6/10 5:35 PM
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

	String value() default "";
}
