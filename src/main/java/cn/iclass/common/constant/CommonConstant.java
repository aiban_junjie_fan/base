package cn.iclass.common.constant;

/**
 * @Description: 常量
 * @Author: alan
 * @Date: 2019/6/10 5:38 PM
 */
public class CommonConstant {
	/**
	 * 超级管理员ID
	 */
	public static final int SUPER_ADMIN = 1;
	/**
	 * 默认分页开始页码
	 */
	public static final Integer DEFAULT_PAGE_INDEX = 1;
	/**
	 * 默认分页每页页数
	 */
	public static final Integer DEFAULT_PAGE_SIZE = 10;

	/**
	 * @Description: 菜单类型
	 * @Author: alan
	 * @Date: 2019/6/10 5:49 PM
	 */
	public enum MenuType {
		/**
		 * 目录
		 */
		CATALOG(0),
		/**
		 * 菜单
		 */
		MENU(1),
		/**
		 * 按钮
		 */
		BUTTON(2);

		private int value;

		MenuType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

}
