package cn.iclass.common.constant;

/**
 * Redis所有Keys
 *
 * @author alan
 * @date 2017-07-18 19:51
 */
public class RedisKeys {

	public final static String DATABASE = "base";
	public final static String SEPARATOR = "::";
	public final static String SYS_CONFIG = DATABASE + SEPARATOR + "sys:config:";
	public final static String SYS_USER_TOKEN = DATABASE + SEPARATOR + "sys:user:token:";
	public final static String CAPTCHA = DATABASE + SEPARATOR + "captcha:";
	public final static String SIMPLE_TEST = DATABASE + SEPARATOR + "simple:test:";
}
