package cn.iclass.common.utils;

import java.util.HashMap;


/**
 * @Description: Map工具类
 * @Author: alan
 * @Date: 2019/6/10 5:47 PM
 */
public class MapUtils extends HashMap<String, Object> {

	@Override
	public MapUtils put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
