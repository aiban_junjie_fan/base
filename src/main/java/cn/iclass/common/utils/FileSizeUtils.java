package cn.iclass.common.utils;

import java.text.DecimalFormat;

/**
 * @Description: FileSizeUtils.java
 * @author: alan
 * @date: 2019/8/5 4:04 PM
 */
public class FileSizeUtils {


	/**
	 * 格式化文件大小
	 *
	 * @param size
	 * @return java.lang.String
	 * @author alan
	 * @date 2019/8/5 4:04 PM
	 */
	public static String format(long size) {
		if (size <= 0) {
			return "0 B";
		}
		final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
}
