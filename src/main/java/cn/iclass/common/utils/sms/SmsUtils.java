package cn.iclass.common.utils.sms;


import cn.hutool.json.JSONUtil;
import cn.iclass.common.enums.SmsTemplateEnum;
import cn.iclass.common.exception.GlobalException;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 阿里云短信工具类
 * @author: alan
 * @date: 2019/7/8 10:16 AM
 */
@Slf4j
public class SmsUtils {

	/**
	 * 产品名称:云通信短信API产品,开发者无需替换
	 */
	private static final String DOMAIN = "dysmsapi.aliyuncs.com";


	private static String accessKeyId = "LTAI4Fx7gp9Juno2vPgaopN1";

	private static String accessKeySecret = "Z9tBK1OF391MtwmJyuQM6eDi8nl0Uv";

	private static String signName = "爱班";
	private static String PRODUCT_NAME = "VIPclass";
	private static String LIMIT_CODE = "isv.BUSINESS_LIMIT_CONTROL";

	private static SmsUtils ourInstance = new SmsUtils();
	private static IAcsClient client;

	private SmsUtils() {
		init();
	}

	public static SmsUtils getInstance() {
		return ourInstance;
	}

	/**
	 * sendMsg
	 *
	 * @param mobile
	 * @param code
	 * @param templateEnum
	 * @return boolean
	 * @author alan
	 * @date 2019/12/17 14:13
	 */
	public static boolean sendMsg(String mobile, String code, SmsTemplateEnum templateEnum) {
		return sendMsg(mobile, templateEnum, "{\"code\":\"" + code + "\",\"product\":\"" + PRODUCT_NAME + "\"}");
	}

	/**
	 * sendMsg
	 *
	 * @param mobile
	 * @param templateEnum
	 * @param templateParam
	 * @return boolean
	 * @author alan
	 * @date 2019/12/17 14:07
	 */
	public static boolean sendMsg(String mobile, SmsTemplateEnum templateEnum, String templateParam) {
		CommonRequest request = new CommonRequest();
		request.putQueryParameter("PhoneNumbers", mobile);
		request.putQueryParameter("TemplateCode", templateEnum.getDesc());
		request.putQueryParameter("TemplateParam", templateParam);
		Boolean res = send(request);
		if (res != null) {
			return res;
		}
		return false;
	}

	private static Boolean send(CommonRequest request) {
		request.setProtocol(ProtocolType.HTTPS);
		request.setDomain(DOMAIN);
		request.setVersion("2017-05-25");
		request.setAction("SendSms");
		request.putQueryParameter("SignName", signName);
		request.putQueryParameter("RegionId", "cn-hangzhou");
		try {
			CommonResponse response = client.getCommonResponse(request);
			SmsResponse res = JSONUtil.toBean(response.getData(), SmsResponse.class);
			log.info(response.getData());
			log.info(res.toString());
			if (LIMIT_CODE.equalsIgnoreCase(res.getCode())) {
				throw new GlobalException("您发送的频率过于频繁，请稍候重试");
			}
			return "OK".equalsIgnoreCase(res.getCode());
		} catch (ServerException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
		return null;
	}


	private void init() {
		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
		client = new DefaultAcsClient(profile);
	}
}
