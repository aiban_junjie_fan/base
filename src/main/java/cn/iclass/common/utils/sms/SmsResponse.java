package cn.iclass.common.utils.sms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Description: 阿里云短信返回结果
 * @Author: alan
 * @Date: 2019/7/8 10:19 AM
 */
@Data
public class SmsResponse {
	@JsonProperty("Message")
	private String Message;
	@JsonProperty("RequestId")
	private String RequestId;
	@JsonProperty("BizId")
	private String BizId;
	@JsonProperty("Code")
	private String Code;
}
