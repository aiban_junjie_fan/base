package cn.iclass.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 *
 * @author alan
 * @date 2016年11月4日 下午12:59:00
 */
@Setter
@Getter
public class PageUtils<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 总记录数
	 */
	private int totalCount;
	/**
	 * 每页记录数
	 */
	private int pageSize;
	/**
	 * 总页数
	 */
	private int totalPage;
	/**
	 * 当前页数
	 */
	private int currPage;
	/**
	 * 列表数据
	 */
	private List<?> list;

	/**
	 * 分页
	 *
	 * @param list       列表数据
	 * @param totalCount 总记录数
	 * @param pageSize   每页记录数
	 * @param currPage   当前页数
	 */
	public PageUtils(List<?> list, int totalCount, int pageSize, int currPage) {
		this.list = list;
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.currPage = currPage;
		this.totalPage = (int) Math.ceil((double) totalCount / pageSize);
	}

	/**
	 * 分页
	 *
	 * @param list
	 * @param total
	 * @param size
	 * @param current
	 * @param pages
	 * @return
	 * @author alan
	 * @date 2019/7/30 2:01 PM
	 */
	public PageUtils(List<?> list, long total, long size, long current, long pages) {
		this.list = list;
		this.totalCount = (int) total;
		this.pageSize = (int) size;
		this.currPage = (int) current;
		this.totalPage = (int) pages;
	}

	/**
	 * 分页
	 *
	 * @param page
	 */
	public PageUtils(IPage<?> page) {
		this.list = page.getRecords();
		this.totalCount = (int) page.getTotal();
		this.pageSize = (int) page.getSize();
		this.currPage = (int) page.getCurrent();
		this.totalPage = (int) page.getPages();
	}

}
