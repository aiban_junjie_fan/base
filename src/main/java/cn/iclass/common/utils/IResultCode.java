package cn.iclass.common.utils;

import java.io.Serializable;

/**
 * @description: 状态码接口
 * @author: alan
 * @date: 2019/10/14 10:57
 */
public interface IResultCode extends Serializable {
	/**
	 * 返回的code码
	 *
	 * @return code
	 */
	int getCode();

	/**
	 * 返回的消息
	 *
	 * @return 消息
	 */
	String getMsg();
}

