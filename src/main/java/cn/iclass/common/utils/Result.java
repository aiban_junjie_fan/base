package cn.iclass.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import java.io.Serializable;

/**
 * @Description: 响应信息主体
 * @Author: alan
 * @Date: 2019/7/13 19:23
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(description = "响应信息主体")
public class Result<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@ApiModelProperty(value = "返回标记：成功标记=200，失败标记=500")
	private int code;

	@Getter
	@Setter
	@ApiModelProperty(value = "返回信息")
	private String msg;


	@Getter
	@Setter
	@ApiModelProperty(value = "数据")
	private T data;

	public static <T> Result<T> ok() {
		return restResult(null, HttpStatus.SC_OK, null);
	}

	public static <T> Result<T> ok(T data) {
		return restResult(data, HttpStatus.SC_OK, null);
	}

	public static <T> Result<T> msg(String msg) {
		return restResult(null, HttpStatus.SC_OK, msg);
	}

	public static <T> Result<T> ok(T data, String msg) {
		return restResult(data, HttpStatus.SC_OK, msg);
	}

	public static <T> Result<T> error() {
		return restResult(null, HttpStatus.SC_INTERNAL_SERVER_ERROR, null);
	}

	public static <T> Result<T> error(String msg) {
		return restResult(null, HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
	}

	public static <T> Result<T> error(T data) {
		return restResult(data, HttpStatus.SC_INTERNAL_SERVER_ERROR, null);
	}

	public static <T> Result<T> error(int code, String msg) {
		return restResult(null, code, msg);
	}

	public static <T> Result<T> error(IResultCode code) {
		return restResult(null, code.getCode(), code.getMsg());
	}

	private static <T> Result<T> restResult(T data, int code, String msg) {
		Result<T> apiResult = new Result<>();
		apiResult.setCode(code);
		apiResult.setData(data);
		apiResult.setMsg(msg);
		return apiResult;
	}
}
