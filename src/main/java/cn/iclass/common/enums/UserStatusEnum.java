package cn.iclass.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import io.swagger.annotations.ApiModel;

/**
 * @description: UserStatusEnum.java
 * @author: alan
 * @date: 2019/11/19 11:32
 */
@ApiModel
public enum UserStatusEnum {

	/**
	 * 正常
	 */
	ENABLED(0, "正常"),
	/**
	 * 禁用
	 */
	DISABLED(1, "禁用"),

	;

	@EnumValue
	private int value;
	private String desc;

	UserStatusEnum(final int value, final String desc) {
		this.value = value;
		this.desc = desc;
	}

	public int getValue() {
		return value;
	}

	public String getDesc() {
		return desc;
	}
}