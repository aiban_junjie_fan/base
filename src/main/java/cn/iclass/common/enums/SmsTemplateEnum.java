package cn.iclass.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import io.swagger.annotations.ApiModel;

/**
 * @Author: alan
 * @Date: 2018/6/5
 */
@ApiModel("短信发送模板")
public enum SmsTemplateEnum {

	/**
	 * 用户注册
	 */
	用户注册(1, "SMS_67165428"),
	/**
	 * 登录确认
	 */
	登录确认(2, "SMS_67165430"),
	/**
	 * 身份验证
	 */
	身份验证(3, "SMS_67165432"),
	/**
	 * 修改密码
	 */
	修改密码(3, "SMS_67165426"),
	;

	@EnumValue
	private int value;
	private String desc;

	SmsTemplateEnum(final int value, final String desc) {
		this.value = value;
		this.desc = desc;
	}


	public int getValue() {
		return value;
	}

	public String getDesc() {
		return desc;
	}
}