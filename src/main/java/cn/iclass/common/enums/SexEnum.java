package cn.iclass.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import io.swagger.annotations.ApiModel;

/**
 * @Author: alan
 * @Date: 2018/6/5
 */
@ApiModel
public enum SexEnum {
	/**
	 * 女
	 */
	FEMALE(1, "女"),


	/**
	 * 男
	 */
	MALE(2, "男");

	@EnumValue
	private int value;
	private String desc;

	SexEnum(final int value, final String desc) {
		this.value = value;
		this.desc = desc;
	}

	public int getValue() {
		return value;
	}

	public String getDesc() {
		return desc;
	}
}