package cn.iclass.common.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 方便后续拓展自己的业务逻辑
 * @Author: alan
 * @Date: 2019/6/10 5:37 PM
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {

}
