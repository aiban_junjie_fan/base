package cn.iclass.common.mp;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Description: 填充器
 * @Author: alan
 * @Date: 2019/6/10 6:03 PM
 */
@Component
public class IMetaObjectHandler implements MetaObjectHandler {

	@Override
	public void insertFill(MetaObject metaObject) {
		this.setFieldValByName("deleted", 0, metaObject);
		this.setFieldValByName("version", 0, metaObject);
		this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
		this.setFieldValByName("lastModifyTime", LocalDateTime.now(), metaObject);
	}

	@Override
	public void updateFill(MetaObject metaObject) {

		this.setFieldValByName("lastModifyTime", LocalDateTime.now(), metaObject);

	}

}
