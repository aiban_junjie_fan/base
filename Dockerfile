FROM reg.iclass.cn/iclass.cn/jdk8:sox

ADD target/*.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom" ,"-jar"  ,"/app.jar"]