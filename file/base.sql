-- -------------------------------------------------------------
-- TablePlus 2.11(276)
--
-- https://tableplus.com/
--
-- Database: base
-- Generation Time: 2019-11-19 11:38:28.7330
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统日志';

DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='菜单管理';

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='角色';

DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `salt` varchar(100) DEFAULT NULL COMMENT '随机盐',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `name` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `mobile` varchar(20) DEFAULT NULL COMMENT '电话',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建人',
  `status` int(8) DEFAULT '0' COMMENT '用户状态',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统用户';

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `deleted` int(8) DEFAULT '0' COMMENT '逻辑删除',
  `version` int(11) DEFAULT '0' COMMENT '乐观锁',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`, `deleted`, `version`, `create_time`, `last_modify_time`) VALUES ('1', '0', '系统管理', NULL, NULL, '0', 'system', '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('2', '1', '管理员列表', 'sys/user', NULL, '1', 'admin', '1', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('3', '1', '角色管理', 'sys/role', NULL, '1', 'role', '2', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('4', '1', '菜单管理', 'sys/menu', NULL, '1', 'menu', '3', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('5', '1', 'SQL监控', 'http://localhost:8089/manage/api/druid/sql.html', NULL, '1', 'sql', '4', '0', '0', '2018-10-10 14:31:03', '2019-06-10 23:04:02'),
('6', '1', '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'config', '6', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('7', '1', '系统日志', 'sys/log', 'sys:log:list', '1', 'log', '7', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('8', '2', '查看', NULL, 'sys:user:list,sys:user:info', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('9', '2', '新增', NULL, 'sys:user:save,sys:role:select', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('10', '2', '修改', NULL, 'sys:user:update,sys:role:select', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('11', '2', '删除', NULL, 'sys:user:delete', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('12', '3', '查看', NULL, 'sys:role:list,sys:role:info', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('13', '3', '新增', NULL, 'sys:role:save,sys:menu:list', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('14', '3', '修改', NULL, 'sys:role:update,sys:menu:list', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('15', '3', '删除', NULL, 'sys:role:delete', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('16', '4', '查看', NULL, 'sys:menu:list,sys:menu:info', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('17', '4', '新增', NULL, 'sys:menu:save,sys:menu:select', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('18', '4', '修改', NULL, 'sys:menu:update,sys:menu:select', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03'),
('19', '4', '删除', NULL, 'sys:menu:delete', '2', NULL, '0', '0', '0', '2018-10-10 14:31:03', '2018-10-10 14:31:03');

INSERT INTO `sys_user` (`user_id`, `username`, `salt`, `password`, `name`, `mobile`, `avatar`, `create_time`, `deleted`, `version`, `last_modify_time`, `create_user_id`, `status`) VALUES ('1', 'admin', 'YzcmCZNvbXocrsz9dm8e', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'admin', '18268617818', NULL, '2016-11-11 11:11:11', '0', '1', '2019-06-11 00:59:24', NULL, '0');




/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;